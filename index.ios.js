'use strict';

var React       = require('react-native');
var Icon        = require('react-native-vector-icons/FontAwesome');

var Services  = require('./app/pages/Services');
var Projects  = require('./app/pages/Projects');
var Settings  = require('./app/pages/Settings');

var {
  AppRegistry,
  NavigatorIOS,
  StyleSheet,
  TabBarIOS,
} = React;

var dado_ui = React.createClass({
  getInitialState: function() {
    return {
      selectedTab: 'new'
    };
  },

  render: function() {
    return (
      <TabBarIOS>
        <Icon.TabBarItem
          title="New"
          iconName="plus-square"
          selectedIconName="plus-square"
          selected={this.state.selectedTab === 'new'}
          onPress={() => {
            this.setState({
              selectedTab: 'new',
            });
          }}>
          <NavigatorIOS style={styles.container}
              initialRoute={{
                title: 'Services',
                component: Services
              }} 
          />
        </Icon.TabBarItem>

        <Icon.TabBarItem
          title="Parse"
          iconName="calendar"
          selectedIconName="calendar"
          selected={this.state.selectedTab === 'projects'}
          onPress={() => {
            this.setState({
              selectedTab: 'projects',
            });
          }}>
          <NavigatorIOS style={styles.container}
              initialRoute={{
                title: 'My Projects',
                component: Projects
              }} 
          />
        </Icon.TabBarItem>

        <Icon.TabBarItem
          title="Facebook"
          iconName="user"
          selectedIconName="user"
          selected={this.state.selectedTab === 'setting'}
          onPress={() => {
            this.setState({
              selectedTab: 'setting',
            });
          }}>
          <NavigatorIOS style={styles.container}
              initialRoute={{
                title: 'Setting',
                component: Settings
              }} 
          />
        </Icon.TabBarItem>
      </TabBarIOS>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabContent: {
    flex: 1,
    alignItems: 'center',
  },
  tabText: {
    color: 'white',
    margin: 50,
  },
  wrapper: {
    flex: 1
  }
});

AppRegistry.registerComponent('dado_ui', () => dado_ui);

module.exports = dado_ui;