'use strict';

var React = require('react-native');
var Packages = require('./Packages');
var WithLabel = require('../components/WithLabel');

var {
  StyleSheet,
  Text,
  View,
  SwitchIOS,
  TextInput,
  Image,
  TouchableHighlight,
} = React;

var Details = React.createClass({
  getInitialState: function() {
    return {
      location: '',
      additional: '',
      switchone: false,
    };
  },

  _onSubmit: function() {
    this.props.navigator.push(
      {
          component: Packages,
          title: `My Packages`,
          passProps: { 
            category: this.props.category,
            location: this.state.location,
            additional: this.state.additional,
            switchone: this.state.switchone, 
          },
      }
    );
  },

  render: function() {
    return (
    	<View style={styles.container}>
        <View style={styles.title}>
          <Text style={styles.titleText}>{this.props.category}</Text>
        </View>
        <View style={styles.headerBlock}>
          <View style={styles.headerContent}>
              <View style={styles.headerDesc}>
                  <Text>{this.props.detail} is a room of a house or an apartment where people sleep. A typical Western bedroom consists of a bed (ranging from a crib for an </Text>
              </View>
              <Image style={styles.headerImage} source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Bedroom_Mitcham.jpg/800px-Bedroom_Mitcham.jpg'}} />
          </View>
        </View>
        <View style={styles.title}>
          <Text style={styles.titleText}>Please fill these questions</Text>
        </View>
        <View style={styles.questionBlock}>
          <WithLabel label="Additinal Info">
              <TextInput
                placeholder="Please fill in more detail"
                multiline={true}
                style={styles.multiline}
                onChangeText={(text) => {
                  this.setState({
                      additional: text
                  })
                }}
              />
          </WithLabel>
          <WithLabel label="Term">
              <SwitchIOS
                  onValueChange={(value) => this.setState({switchone: value})}
                  style={{marginBottom: 10}}
                  value={this.state.switchone} />
          </WithLabel>
          <WithLabel label="Location">
            <TextInput
                autoCapitalize="none"
                placeholder="Please enter project location"
                autoCorrect={false}
                style={styles.textbox}
                onChange={(event) => {
                  this.setState({
                      location: event.nativeEvent.text
                  })
                }}
            />
          </WithLabel>
          <TouchableHighlight style={styles.button} onPress={this._onSubmit} underlayColor='#99d9f4'>
            <Text style={styles.buttonText}>Next</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
});

var styles = StyleSheet.create({
  	container: {
        flex: 1,
        marginTop: 65,
        backgroundColor:'#ebeef0',        
    },
    headerContent:{
        flexDirection: 'row',
        margin: 10,
        justifyContent: 'space-between'
    },
    headerDesc:{
        width: 150,
    },
    headerImage: {
        height: 130,
        width: 130,
    },
    questionBlock: {
        padding: 10,
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        height: 36,
        backgroundColor: '#81c04d',
        borderColor: '#81c04d',
        borderWidth: 1,
        marginTop: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    textbox: {
        height: 26,
        borderWidth: 0.5,
        borderColor: '#0f0f0f',
        flex: 1,
        fontSize: 13,
        padding: 4,
    },
    multiline: {
        borderWidth: 0.5,
        borderColor: '#0f0f0f',
        flex: 1,
        fontSize: 13,
        height: 50,
        padding: 4,
        marginBottom: 4,
    },
    title: {
        padding: 10,
        backgroundColor:'#000000',
    },
    titleText: {
        fontWeight: 'bold',
        fontWeight: '500',
        color: 'white',
    },
});

module.exports = Details;
