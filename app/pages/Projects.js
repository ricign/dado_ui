'use strict';

var React 		= require('react-native');
var Parse       = require('parse').Parse;
var ParseReact  = require('parse-react');

var WithLabel 	= require('../components/WithLabel');

Parse.initialize(
  'HADv6gtwaRcaYOYPZeMIbVHvip4IAt9l8w0nRnJZ',
  'w3CgXKnKB5odjcmNaAuYB97f2NxThkerz1D98j4t'
);

var {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight
} = React;


var Projects = React.createClass({
	mixins: [ParseReact.Mixin],

  observe: function(props, state) {
    return { packages: (new Parse.Query('Packages')) } 
  },

  _onSubmit: function() {
		ParseReact.Mutation.Create('Packages', { name: this.state.name, startAmount: parseInt(this.state.startAmount) }).dispatch();
  },

	render: function() {
    var loadingContent = this.data.packages.length!==0? 
                          <View>
                            {this.data.packages.map(function(c) {
                              return <Text>{c.name} {c.startAmount}</Text>
                              })}
                          </View>
                        :
                          <View><Text>loading from parse ... </Text></View>                     

		return(
			<View style={styles.container}>
				<Text>This is Project placeholder</Text>
        		 <View>
			          <WithLabel label="Package name">
			            <TextInput
			                style={styles.textbox}
			                onChange={(event) => {
			                  this.setState({
			                      name: event.nativeEvent.text
			                  })
			                }}
			            />
			          </WithLabel>

			          <WithLabel label="Start amount">
			            <TextInput
			                style={styles.textbox}
			                onChange={(event) => {
			                  this.setState({
			                      startAmount: event.nativeEvent.text
			                  })
			                }}
			            />
			          </WithLabel>

			          <TouchableHighlight style={styles.button} onPress={this._onSubmit} underlayColor='#99d9f4'>
			            	<Text style={styles.buttonText}>Submit to Parse</Text>
			          </TouchableHighlight>
			        </View>
              {loadingContent}
			</View>
		);
	},
});

var styles = StyleSheet.create({
	container: {
    	flex: 1,
    	marginTop: 65,
  	},
  	buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        height: 36,
        backgroundColor: '#81c04d',
        borderColor: '#81c04d',
        borderWidth: 1,
        marginTop: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    textbox: {
        height: 26,
        borderWidth: 0.5,
        borderColor: '#0f0f0f',
        flex: 1,
        fontSize: 13,
        padding: 4,
    },
});

module.exports = Projects;