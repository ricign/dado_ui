'use strict';

var React  		= require('react-native');
var Swiper		= require('react-native-swiper')
var Collapsible = require('react-native-collapsible');
var Accordion 	= require('react-native-collapsible/Accordion');

var {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight
} = React;

var CONTENT = [
  {
    title: 'Basic',
    cost: '$10,000'
  },
  {
    title: 'Standard',
    cost: '$25,000'
  },
  {
    title: 'Premium',
  	cost: '$100,000'
  },
];

var Packages = React.createClass({
	getInitialState: function() {
	    return {
	      collapsed: true
	    };
  	},

	_renderHeader(section) {
		return (
		  <View style={styles.sliderHeader}>
		    <Text style={styles.headerText}>{section.title}</Text>
		  </View>
		);
	},

	_renderContent(section) {
		return (
		  	<View>
	    		<Swiper style={styles.sliderContainer} height={150} showsButtons={true} showsPagination={false}>
					<View style={styles.slide}>
	            		<Image style={styles.image} source={{uri: 'http://iss.zillowstatic.com/image/modern-master-bedroom-with-wood-ceiling-accent-wall-and-vaulted-ceiling-i_g-ISphq1hgpj5rje1000000000-vwm8R.jpg'}} />
	          		</View>
	          		<View style={styles.slide}>
	            		<Image style={styles.image} source={{uri: 'http://photos1.zillowstatic.com/i_g/IS5aneg8euw0rq1000000000.jpg'}} />
	          		</View>
		          	<View style={styles.slide}>
		            	<Image style={styles.image} source={{uri: 'http://photos2.zillowstatic.com/i_g/IS9lajxgor84l00000000000.jpg'}} />
		          	</View>
	      		</Swiper>
	      		<View style={{padding: 10}}>
					<Text>{section.cost}</Text>
				</View>
		  	</View>
		);
	},

	render: function() {
		return(
			<View style={styles.container}>
				<View style={styles.title}>
					<Text style={styles.titleText}>Available packages for {this.props.category}</Text>
			    </View>
			    <View style={styles.contentContainer}>
					<Text>Additional info: {this.props.additional}</Text>
					<Text>Location: {this.props.location}</Text>
					<Text>Term: {this.props.switchone.toString()}</Text>
				</View>

		        <Accordion
			          sections={CONTENT}
			          renderHeader={this._renderHeader}
			          renderContent={this._renderContent}
			          duration={600}
        		/>
	      	</View>
		);
	},
});

var styles = StyleSheet.create({
	container: {
    	flex: 1,
    	marginTop: 65,
    	backgroundColor:'#ebeef0', 
  	},
  	title: {
  		padding: 10,
  		backgroundColor:'#000000',
  	},
  	titleText: {
	    fontWeight: 'bold',
	    fontWeight: '500',
	    color: 'white',
  	},
  	contentContainer: {
  		padding: 10,
  		margin: 10,
  		lineHeight: 10,
  	},
  	sliderHeader: {
		backgroundColor:'#81c04d',
		padding: 10,
  	},
  	headerText: {
	    textAlign: 'center',
	    fontSize: 16,
	    fontWeight: '500',
	    color: 'white',
    },
	slider: {
	    flex: 1,
	    justifyContent: 'center',
	    alignItems: 'center',
	    backgroundColor: '#ebeef0',
  	},
  	image: {
   		flex: 1,
   		height: 150,
   	},
});

module.exports = Packages;