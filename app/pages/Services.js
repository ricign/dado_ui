'use strict';

var React   = require('react-native');
var Icon    = require('react-native-vector-icons/FontAwesome');
var Details = require('./Details');


var {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight
} = React;

var Services = React.createClass({
  
  _onListPress: function(category) {
    console.log("Go to page: ",category);  
    this.props.navigator.push(
      {
          component: Details,
          title: `Details`,
          passProps: { category: category },
      }
    );
  },

  _onButtonPress: function(detail) {  
    this.props.navigator.push(
      {
          component: Details,
          title: `Contact Us`,
      }
    );
  },

  render: function() {
    return (
          <View style={styles.container}>
                <View style={styles.content}>  
                   <TouchableOpacity onPress={()=>this._onListPress('Bedroom')}>   
                      <View style={styles.category} >
                          <Icon name="bed" style={styles.catIcon} />  
                          <Text style={styles.categoryText}>Bedroom</Text>
                      </View>
                   </TouchableOpacity>  
                    <TouchableOpacity onPress={()=>this._onListPress("Living")}>   
                      <View style={styles.category} >
                          <Icon name="tv" style={styles.catIcon} />  
                          <Text style={styles.categoryText}>Living</Text>
                      </View>
                   </TouchableOpacity> 
                </View>
          </View>
    );
  }
});

var styles = StyleSheet.create({
    container:{
        flex:1,
        marginTop: 65, 
        backgroundColor:'#ebeef0',            
    },
    content:{
        flex:1,    
        flexDirection:'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
    },
    category:{
        backgroundColor:'#81c04d',
        width: 120,
        height: 120,
        margin: 20,
        borderRadius: 8,
    },
    catIcon:{
        fontSize: 25,
        color: '#ffffff',
        justifyContent: 'center',
        marginTop: 40, 
    },
    categoryText:{
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold'
    },
});

module.exports = Services;