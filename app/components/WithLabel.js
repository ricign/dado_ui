'use strict';

var React = require('react-native');

var {
  StyleSheet,
  Text,
  View,
} = React;

var WithLabel = React.createClass({
  render: function() {
    return (
      <View style={styles.labelContainer}>
        <View style={styles.label}>
          <Text>{this.props.label}</Text>
        </View>
        {this.props.children}
      </View>
    );
  },
});

var styles = StyleSheet.create({
    labelContainer: {
        flexDirection: 'row',
        marginVertical: 2,
        flex: 1,
    },
    label: {
        width: 115,
        alignItems: 'flex-end',
        marginRight: 10,
        paddingTop: 2,
    },
});

module.exports = WithLabel;